%{
#include "color_converter.tab.h"
#include <string.h>
%}

%%

"#"([0-9A-Fa-f]{3}|[0-9A-Fa-f]{6})      { yylval.stringval = strdup(yytext); return HEX_VAL; }
rgb\([0-9]+,[ ]?[0-9]+,[ ]?[0-9]+\)     { yylval.stringval = strdup(yytext); return RGB_VAL; }
hsl\([0-9]+,[ ]?[0-9]+%,[ ]?[0-9]+%\)   { yylval.stringval = strdup(yytext); return HSL_VAL; }
hsv\([0-9]+,[ ]?[0-9]+%,[ ]?[0-9]+%\)   { yylval.stringval = strdup(yytext); return HSV_VAL; }
(HEX|hex)                               { return HEX; }
(RGB|rgb)                               { return RGB; }
(HSV|hsv)                               { return HSV; }
(HSL|hsl)                               { return HSL; }
(TODOS|todos)                           { return TODOS; }
(PARA|para|TO|to)                       { return PARA; }
(HELP|help|AJUDA|ajuda)                 { return AJUDA; }
(SAIR|sair|EXIT|exit)                   { return SAIR; }
,                                       { return COMMA; }
\n                                      { return EOL; }
[ \t]                                   { /* ignorar espaços em branco e tabulações */ }
.                                       { /* ignorar qualquer outro caractere inválido */ }

%%

int yywrap() {
    return 1;
}