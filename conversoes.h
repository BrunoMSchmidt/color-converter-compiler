#ifndef CONVERSOES_H
#define CONVERSOES_H

#include <stdlib.h>
#include <stdio.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct rgbStruct {
    int r;
    int g;
    int b;
};

struct hslStruct {
    int h;
    int s;
    int l;
};

struct hsvStruct {
    int h;
    int s;
    int v;
};

void mensagemInicial();
void mensagemAjuda();

// FORMAT
char *formatRgb(struct rgbStruct rgb);
char *formatHsl(struct hslStruct hsl);
char *formatHsv(struct hsvStruct hsv);

// PARSES
char *inputToHex(char *input);
struct rgbStruct inputToRgb(char *rgb);
struct hslStruct inputToHsl(char *hsl);
struct hsvStruct inputToHsv(char *hsv);

// RGB
char *rgbToHex(struct rgbStruct rgb);
struct hslStruct rgbToHsl(struct rgbStruct rgb);
struct hsvStruct rgbToHsv(struct rgbStruct rgb);

// HSV
struct rgbStruct hsvToRgb(struct hsvStruct hsv);
char *hsvToHex(struct hsvStruct hsv);
struct hslStruct hsvToHsl(struct hsvStruct hsv);

// HSL
struct rgbStruct hslToRgb(struct hslStruct hsl);
char *hslToHex(struct hslStruct hsl);
struct hsvStruct hslToHsv(struct hslStruct hsl);

// HEX
struct rgbStruct hexToRgb(char *hex);
struct hslStruct hexToHsl(char *hex);
struct hsvStruct hexToHsv(char *hex);

#endif