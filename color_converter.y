%{
#include <stdio.h>
#include "conversoes.h"

extern int yylex();
extern int yyerror(const char* msg);
%}

%union {
    char* stringval;
}

%token <stringval> STRING HEX_VAL RGB_VAL HSV_VAL HSL_VAL
%token COMMA EOL DE PARA AJUDA SAIR
%token HEX RGB HSV HSL TODOS

%%

input:
    | input line

line: 
    EOL {
        printf("\n");
    } |
    AJUDA {
        mensagemAjuda();
    } |
    SAIR {
        exit(0);
    } |
    RGB_VAL PARA HEX {
        printf("HEX: %s\n", rgbToHex(inputToRgb($1)));
    } |
    RGB_VAL PARA HSV {
        printf("HSV: %s\n", formatHsv(rgbToHsv(inputToRgb($1))));
    } |
    RGB_VAL PARA HSL {
        printf("HSL: %s\n", formatHsl(rgbToHsl(inputToRgb($1))));
    } |
    RGB_VAL PARA TODOS {
        printf("HEX: %s\n", rgbToHex(inputToRgb($1)));
        printf("HSV: %s\n", formatHsv(rgbToHsv(inputToRgb($1))));
        printf("HSL: %s\n", formatHsl(rgbToHsl(inputToRgb($1))));
    } |
    HSV_VAL PARA HEX {
        printf("HEX: %s\n", hsvToHex(inputToHsv($1)));
    } |
    HSV_VAL PARA RGB {
        printf("RGB: %s\n", formatRgb(hsvToRgb(inputToHsv($1))));
    } |
    HSV_VAL PARA HSL {
        printf("HSL: %s\n", formatHsl(hsvToHsl(inputToHsv($1))));
    } |
    HSV_VAL PARA TODOS {
        printf("HEX: %s\n", hsvToHex(inputToHsv($1)));
        printf("RGB: %s\n", formatRgb(hsvToRgb(inputToHsv($1))));
        printf("HSL: %s\n", formatHsl(hsvToHsl(inputToHsv($1))));
    } |
    HSL_VAL PARA HEX {
        printf("HEX: %s\n", hslToHex(inputToHsl($1)));
    } |
    HSL_VAL PARA RGB {
        printf("RGB: %s\n", formatRgb(hslToRgb(inputToHsl($1))));
    } |
    HSL_VAL PARA HSV {
        printf("HSV: %s\n", formatHsv(hslToHsv(inputToHsl($1))));
    } |
    HSL_VAL PARA TODOS {
        printf("HEX: %s\n", hslToHex(inputToHsl($1)));
        printf("RGB: %s\n", formatRgb(hslToRgb(inputToHsl($1))));
        printf("HSV: %s\n", formatHsv(hslToHsv(inputToHsl($1))));
    } |
    HEX_VAL PARA RGB {
        printf("RGB: %s\n", formatRgb(hexToRgb(inputToHex($1))));
    } |
    HEX_VAL PARA HSV {
        printf("HSV: %s\n", formatHsv(hexToHsv(inputToHex($1))));
    } |
    HEX_VAL PARA HSL {
        printf("HSL: %s\n", formatHsl(hexToHsl(inputToHex($1))));
    } |
    HEX_VAL PARA TODOS {
        printf("RGB: %s\n", formatRgb(hexToRgb(inputToHex($1))));
        printf("HSV: %s\n", formatHsv(hexToHsv(inputToHex($1))));
        printf("HSL: %s\n", formatHsl(hexToHsl(inputToHex($1))));
    }
%%

int main() {
    mensagemInicial();

    yyparse();
    return 0;
}

int yyerror(const char* msg) {
    fprintf(stderr, "Erro: %s\n", msg);
    return 1;
}