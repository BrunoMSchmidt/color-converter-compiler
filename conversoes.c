#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

struct rgbStruct {
    int r;
    int g;
    int b;
};

struct hslStruct {
    int h;
    int s;
    int l;
};

struct hsvStruct {
    int h;
    int s;
    int v;
};

void mensagemAjuda() {
    printf("Ajuda:\n\n");
    printf("Para converter uma cor, digite-a no formato RGB, HSV, HSL ou HEX e especifique o formato de destino.\n\n");
    printf("Exemplos de cores:\n");
    printf("RGB: rgb(255, 255, 255)\n");
    printf("HSV: hsv(50, 80%%, 80%%)\n");
    printf("HSL: hsl(50, 80%%, 80%%)\n");
    printf("HEX: #d3f130\n\n");
    printf("Exemplo de uso:\n");
    printf("rgb(255, 255, 255) para hex\n");
    printf("rgb(255, 255, 255) para hsv\n");
    printf("rgb(255, 255, 255) para hsl\n");
    printf("rgb(255, 255, 255) para todos\n\n");
    printf("Digite 'sair' para encerrar o programa.\n\n");
}

void mensagemInicial() {
    printf("Bem-vindo ao Compilador de Conversao de Formato de Cores!\n\n");
    printf("Este compilador foi desenvolvido para auxilia-lo na conversao entre os formatos de cores RGB, HSV, HSL e HEX. Com ele, voce pode facilmente converter cores de um formato para outro.\n\n");
    printf("Para comecar, digite uma cor no formato RGB, HSV, HSL ou HEX e especifique o formato de destino para a conversao. Voce tambem pode digitar 'ajuda' para obter mais informacoes sobre o uso do compilador ou 'sair' para encerrar o programa.\n\n");
    printf("Exemplo de uso:\n");
    printf("rgb(255, 255, 255) para hex\n");
    printf("rgb(255, 255, 255) para hsv\n");
    printf("rgb(255, 255, 255) para hsl\n");
    printf("rgb(255, 255, 255) para todos\n\n");
    printf("Vamos comecar!\n\n");
}

// FORMAT
char* formatRgb(struct rgbStruct rgb) {
    char* rgb1 = malloc(30 * sizeof(char));
    sprintf(rgb1, "rgb(%d, %d, %d)", rgb.r, rgb.g, rgb.b);
    return rgb1;
}

char* formatHsl(struct hslStruct hsl) {
    char* hsl1 = malloc(30 * sizeof(char));
    sprintf(hsl1, "hsl(%d, %d%%, %d%%)", hsl.h, hsl.s, hsl.l);
    return hsl1;
}

char* formatHsv(struct hsvStruct hsv) {
    char* hsv1 = malloc(30 * sizeof(char));
    sprintf(hsv1, "hsv(%d, %d%%, %d%%)", hsv.h, hsv.s, hsv.v);
    return hsv1;
}

// PARSES

char* inputToHex(char* input) {
    if (input[0] == '#') {
        if (strlen(input) == 4) {
            char* hex = malloc(7 * sizeof(char));
            sprintf(hex, "#%c%c%c%c%c%c", input[1], input[1], input[2], input[2], input[3], input[3]);
            return hex;
        } else {
            return input;
        }
    } else {
        return input;
    }
}

struct rgbStruct inputToRgb(char* rgb) {
    struct rgbStruct rgb1;
    sscanf(rgb, "rgb(%d, %d, %d)", &rgb1.r, &rgb1.g, &rgb1.b);
    return rgb1;
}

struct hslStruct inputToHsl(char* hsl) {
    struct hslStruct hsl1;
    sscanf(hsl, "hsl(%d, %d%%, %d%%)", &hsl1.h, &hsl1.s, &hsl1.l);
    return hsl1;
}

struct hsvStruct inputToHsv(char* hsv) {
    struct hsvStruct hsv1;
    sscanf(hsv, "hsv(%d, %d%%, %d%%)", &hsv1.h, &hsv1.s, &hsv1.v);
    return hsv1;
}


// RGB

char* rgbToHex(struct rgbStruct rgb) {
    char* hex = malloc(7 * sizeof(char));
    sprintf(hex, "#%02X%02X%02X", rgb.r, rgb.g, rgb.b);
    return hex;
}

struct hslStruct rgbToHsl(struct rgbStruct rgb) {
    int r = rgb.r;
    int g = rgb.g;
    int b = rgb.b;

    double r1 = r / 255.0;
    double g1 = g / 255.0;
    double b1 = b / 255.0;
    double max = r1;
    if (g1 > max) max = g1;
    if (b1 > max) max = b1;
    double min = r1;
    if (g1 < min) min = g1;
    if (b1 < min) min = b1;
    double h, s, l = (max + min) / 2.0;
    double d = max - min;
    s = max == 0.0 ? 0.0 : d / (1.0 - fabs(2.0 * l - 1.0));
    if (max == min) {
        h = 0.0;
    } else {
        if (max == r1) {
            h = (g1 - b1) / d + (g1 < b1 ? 6.0 : 0.0);
        } else if (max == g1) {
            h = (b1 - r1) / d + 2.0;
        } else {
            h = (r1 - g1) / d + 4.0;
        }
        h /= 6.0;
    }
    struct hslStruct hsl1;
    hsl1.h = (int)(h * 360);
    hsl1.s = (int)(s * 100);
    hsl1.l = (int)(l * 100);
    return hsl1;
}

struct hsvStruct rgbToHsv(struct rgbStruct rgb) {
    int r = rgb.r;
    int g = rgb.g;
    int b = rgb.b;

    double r1 = r / 255.0;
    double g1 = g / 255.0;
    double b1 = b / 255.0;
    double max = r1;
    if (g1 > max) max = g1;
    if (b1 > max) max = b1;
    double min = r1;
    if (g1 < min) min = g1;
    if (b1 < min) min = b1;
    double h, s, v = max;
    double d = max - min;
    s = max == 0.0 ? 0.0 : d / max;
    if (max == min) {
        h = 0.0;
    } else {
        if (max == r1) {
            h = (g1 - b1) / d + (g1 < b1 ? 6.0 : 0.0);
        } else if (max == g1) {
            h = (b1 - r1) / d + 2.0;
        } else {
            h = (r1 - g1) / d + 4.0;
        }
        h /= 6.0;
    }
    struct hsvStruct hsv1;
    hsv1.h = (int)(h * 360);
    hsv1.s = (int)(s * 100);
    hsv1.v = (int)(v * 100);
    return hsv1;
}


// HSV

struct rgbStruct hsvToRgb(struct hsvStruct hsv) {
    int h = hsv.h;
    int s = hsv.s;
    int v = hsv.v;

    double h1 = h / 360.0;
    double s1 = s / 100.0;
    double v1 = v / 100.0;
    double r, g, b;
    if (s1 == 0.0) {
        r = g = b = v1;
    } else {
        double h6 = h1 * 6.0;
        double h6mod2 = fmod(h6, 2.0);
        double f = h6 - h6mod2;
        double p = v1 * (1.0 - s1);
        double q = v1 * (1.0 - s1 * h6mod2);
        double t = v1 * (1.0 - s1 * (1.0 - h6mod2));
        if (f == 0.0) {
            r = v1;
            g = t;
            b = p;
        } else if (f == 1.0) {
            r = q;
            g = v1;
            b = p;
        } else if (f == 2.0) {
            r = p;
            g = v1;
            b = t;
        } else if (f == 3.0) {
            r = p;
            g = q;
            b = v1;
        } else if (f == 4.0) {
            r = t;
            g = p;
            b = v1;
        } else if (f == 5.0) {
            r = v1;
            g = p;
            b = q;
        }
    }
    struct rgbStruct rgb1;
    rgb1.r = (int)(r * 255);
    rgb1.g = (int)(g * 255);
    rgb1.b = (int)(b * 255);
    return rgb1;
}

char* hsvToHex(struct hsvStruct hsv) {
    struct rgbStruct rgb = hsvToRgb(hsv);
    return rgbToHex(rgb);
}

struct hslStruct hsvToHsl(struct hsvStruct hsv) {
    struct rgbStruct rgb = hsvToRgb(hsv);
    return rgbToHsl(rgb);
}


// HSL
struct rgbStruct hslToRgb(struct hslStruct hsl) {
    int h = hsl.h;
    int s = hsl.s;
    int l = hsl.l;

    double h1 = h / 360.0;
    double s1 = s / 100.0;
    double l1 = l / 100.0;
    double r, g, b;
    if (s1 == 0.0) {
        r = g = b = l1;
    } else {
        double t2 = l1 < 0.5 ? l1 * (1.0 + s1) : l1 + s1 - l1 * s1;
        double t1 = 2.0 * l1 - t2;
        double h6 = h1 * 6.0;
        double h6mod2 = fmod(h6, 2.0);
        double r1 = h6mod2 < 1.0 ? t2 : h6mod2 < 3.0 ? l1 : h6mod2 < 4.0 ? t1 : t1;
        double g1 = h6mod2 < 1.0 ? h6mod2 * (t2 - t1) + t1 : h6mod2 < 3.0 ? t2 : h6mod2 < 4.0 ? (4.0 - h6mod2) * (t2 - t1) + t1 : t1;
        double b1 = h6mod2 < 1.0 ? t1 : h6mod2 < 3.0 ? t1 : h6mod2 < 4.0 ? h6mod2 * (t2 - t1) + t1 : (6.0 - h6mod2) * (t2 - t1) + t1;
        r = r1;
        g = g1;
        b = b1;
    }
    struct rgbStruct rgb1;
    rgb1.r = (int)(r * 255);
    rgb1.g = (int)(g * 255);
    rgb1.b = (int)(b * 255);
    return rgb1;
}

char* hslToHex(struct hslStruct hsl) {
    struct rgbStruct rgb = hslToRgb(hsl);
    return rgbToHex(rgb);
}

struct hsvStruct hslToHsv(struct hslStruct hsl) {
    struct rgbStruct rgb = hslToRgb(hsl);
    return rgbToHsv(rgb);
}


// HEX
struct rgbStruct hexToRgb(char* hex) {
    int r, g, b;
    sscanf(hex, "#%02X%02X%02X", &r, &g, &b);
    struct rgbStruct rgb1;
    rgb1.r = r;
    rgb1.g = g;
    rgb1.b = b;
    return rgb1;
}

struct hslStruct hexToHsl(char* hex) {
    struct rgbStruct rgb = hexToRgb(hex);
    return rgbToHsl(rgb);
}

struct hsvStruct hexToHsv(char* hex) {
    struct rgbStruct rgb = hexToRgb(hex);
    return rgbToHsv(rgb);
}