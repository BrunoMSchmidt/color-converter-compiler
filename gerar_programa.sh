#!/bin/bash

# Script para rodar os seguintes comandos:

# flex color_converter.l
# bison -d color_converter.y
# gcc -o color_converter color_converter.tab.c lex.yy.c conversoes.c

flex color_converter.l
bison -d color_converter.y
gcc -o color_converter color_converter.tab.c lex.yy.c conversoes.c